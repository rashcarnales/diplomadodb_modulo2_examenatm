-- Crear un tipo de dato para las cuentas
sp_addtype cct, "char(12)"
go


--Crear
DROP TABLE cuentas
go
CREATE TABLE cuentas(
        acct_no cct not null,
        acc_type char(3) not null,
        status varchar(15) not null,
        cur_bal numeric(10,2) not null,
        PRIMARY KEY(acct_no)
)
go



DROP TABLE trans
go


CREATE TABLE trans(
        tran_id int,
        tran_name varchar(21) not null,
        PRIMARY KEY(tran_id)
)
go


--Borrar 
DROP TABLE movs
go


CREATE TABLE movs(
        id_mov numeric(5,0) IDENTITY,
        acct_no cct not null,
        tran_id int,
        fecha_mov date default getdate(),
		monto numeric(10,2) null, 
        PRIMARY KEY(id_mov),
        FOREIGN KEY(acct_no ) REFERENCES cuentas(acct_no),
        FOREIGN KEY(tran_id ) REFERENCES trans(tran_id)
)
go


INSERT INTO trans (tran_id, tran_name ) VALUES (10, 'Deposito')
go
INSERT INTO trans (tran_id, tran_name ) VALUES (20, 'Retiro')
go
INSERT INTO trans (tran_id, tran_name ) VALUES (30, 'Consulta Saldo')
go 
INSERT INTO trans (tran_id, tran_name ) VALUES (40, 'Consulta de Movimientos')
go 
INSERT INTO trans (tran_id, tran_name ) VALUES (50, 'Transferencia')
go

INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1135899-8', 'CH1', 'Closed', 0)
go
INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1273588-4',   	'CH1',	'Active',	786789)
go
INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1273708-9',   'CH1',	'Locked',	12789)
go
INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1274495-6',   	'CH3',	'Active',	8526)
go
INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1274540-5',   	'CH3',	'Unfunded',	0)
go



---------------------Borrado de store procedure
drop proc deposit
go
drop proc withdrawal
go
drop proc checkAccountMoves
go
drop proc atm
go

---------------------Deposito
create proc deposit
@acct cct,
@amonto int
as


declare @tempRowCount1 int
declare @tempRowCount2 int
declare @tempNewMonto numeric(10,2)


INSERT INTO movs (acct_no, tran_id,  monto ) VALUES (@acct,  10, @amonto)
SELECT @tempRowCount1 = @@rowcount
print "@tempRowCount1 %1!", @tempRowCount1


IF @amonto > 0
BEGIN
	UPDATE cuentas set cur_bal = cur_bal + @amonto, status = 'Active' WHERE acct_no=@acct
	SELECT @tempRowCount2 = @@rowcount
	print "@tempRowCount2 %1!", @tempRowCount2

	SELECT acct_no, cur_bal FROM cuentas WHERE acct_no=@acct
	IF @@error <> 0 or (@tempRowCount1 <> 1 AND @tempRowCount2 <> 1)
	BEGIN
		print 'deposito no realizado'
                                
   		return -900
	END
	ELSE
	BEGIN
    	select @tempNewMonto=(select cur_bal from cuentas where acct_no=@acct)
		print "su saldo actual es de %1!", @tempNewMonto
		return 0
	END
END
ELSE
BEGIN
	print "el monto a depositar debe ser mayor a 0"
END 
go

---------------------Retiro
create proc withdrawal
@acct cct,
@amonto int
as


declare @tempRowCount1 int
declare @tempRowCount2 int
declare @tempNewMonto numeric(10,2)


INSERT INTO movs (acct_no, tran_id,  monto ) VALUES (@acct,  20, @amonto)
SELECT @tempRowCount1 = @@rowcount
print "@tempRowCount1 %1!", @tempRowCount1


UPDATE cuentas set cur_bal = cur_bal - @amonto WHERE acct_no=@acct
SELECT @tempRowCount2 = @@rowcount
print "@tempRowCount2 %1!", @tempRowCount2

SELECT acct_no, @tempNewMonto = cur_bal FROM cuentas WHERE acct_no=@acct
IF @@error <> 0 or (@tempRowCount1 <> 1 AND @tempRowCount2 <> 1)
BEGIN
	print 'retiro no realizado'
                                
    return -901
END
ELSE
BEGIN
	IF @tempNewMonto = 0
	BEGIN
		UPDATE cuentas set status = 'Unfunded' WHERE acct_no=@acct
	END
    select @tempNewMonto=(select cur_bal from cuentas where acct_no=@acct)
	print "su saldo actual es de %1!", @tempNewMonto
	return 0
END
go

---------------------Consulta de Movimientos
create proc checkAccountMoves
@acct cct
as

DECLARE movscur CURSOR
	FOR SELECT movs.fecha_mov, movs.monto, trans.tran_name FROM movs, trans WHERE movs.tran_id = trans.tran_id AND movs.acct_no = @acct ORDER BY 1 for read only

declare @fecha date, @monto numeric(10,2), @name varchar(21)
open movscur	
fetch movscur into @fecha, @monto, @name
while @@sqlstatus <> 2
begin
	if @@sqlstatus = 1
	begin
		--raiserror 30001 "select failed"
		 print "Error con el select"
 		 close movscur
 		 deallocate cursor movscur
 		 return
	end
	--fetch movscur into @fecha, @monto, @name
	print "%1! cantidad  %2! transaccion %3! :", @fecha, @monto, @name
	fetch movscur into @fecha, @monto, @name
end
close movscur 
deallocate cursor movscur
go

---------------------ATM
create proc atm 
@acct1 cct, 
@atran int  = NULL,  
@amonto numeric(10,2)  = NULL,  
@acct2 cct  = NULL,  
@rest int  = NULL
as


begin transaction


declare @tempNewMonto numeric(10,2)
declare @tempRowCount int
declare @tempRowCount1 int
declare @tempRowCount2 int
declare @tempAcct_no cct
declare @tempAcc_type char(3)
declare @tempStatus varchar(15) 
declare @tempCur_bal numeric(10,2)


SELECT @tempAcct_no = acct_no, @tempAcc_type = acc_type, @tempStatus = lower(status), @tempCur_bal = cur_bal FROM cuentas WHERE acct_no = @acct1


SELECT @tempRowCount = @@rowcount


IF @tempRowCount=1
BEGIN
    print "existe cuenta"
    IF @atran=10
    BEGIN
		print "deposito"
    	print @tempStatus

    	IF (@tempStatus = 'active' OR @tempStatus = 'unfonded' OR @tempStatus= 'locked' )
        BEGIN
        	declare @restDeposit int
                	
            exec @restDeposit =  deposit @acct1, @amonto
            IF @restDeposit<> 0
            BEGIN
            	print "Error depositando"
            END
            ELSE
            BEGIN
            	print "El deposito se realizo con exito."
            END     
        END
        ELSE IF (@tempStatus = 'closed')
        BEGIN
        	print "La cuenta esta cerrada"
		END
	END
    ELSE IF @atran = 20
    BEGIN
		print "retiro"
        print @tempStatus

        IF (@tempStatus = 'active')
        BEGIN
                
        	IF @tempCur_bal >= @amonto
            BEGIN
            	declare @restWDrawal int
                	
                exec @restWDrawal =  withdrawal @acct1, @amonto
                IF @restWDrawal<> 0
                BEGIN
                	print "Error al hacer el retiro"
                END
                ELSE
                BEGIN
                	print "El retiro se realizo con exito."
                END
            END
            ELSE
            BEGIN
            	print "No cuenta con la cantidad necesaria"
            END
        END  
        ELSE IF (@tempStatus = 'unfonded')
        BEGIN
                	print "La cuenta no tiene fondos"
		END
		ELSE IF (@tempStatus = 'locked')
        BEGIN
            print "La cuenta esta bloqueada"
		END
		ELSE IF (@tempStatus = 'closed')
        BEGIN
            print "La cuenta esta cerrada"
		END
	END
	ELSE IF @atran = 30
	BEGIN
		print "consulta de saldo"
		print "su saldo actual es de %1!", @tempCur_bal
	END        
	ELSE IF @atran = 40
	BEGIN
		print "Consulta movimientos"
    	print @tempStatus

    	IF (@tempStatus = 'active' OR @tempStatus = 'unfonded' OR @tempStatus= 'locked' )
        BEGIN
        	  exec checkAccountMoves @acct1
        END
        ELSE IF (@tempStatus = 'closed')
        BEGIN
        	print "La cuenta esta cerrada"
		END
	END
	ELSE IF @atran = 50
	BEGIN
    	print "transferencia"
        print @tempStatus

        IF (@tempStatus = 'active')
        BEGIN
                
        	IF @tempCur_bal >= @amonto
            BEGIN
            	declare @restTran1 int
                	
                exec @restTran1 =  withdrawal @acct1, @amonto
                IF @restTran1<> 0
                BEGIN
                	print "Error al hacer el retiro de tu cuenta durante la transferecia"
                END
                ELSE
                BEGIN
                
                	declare @restTran2 int
                	exec @restTran2 =  deposit @acct2, @amonto
                	IF @restTran2<> 0
                	BEGIN
                		print "Error al hacer el deposito durante la transferecia"
                	END
                	ELSE
                	BEGIN
                		print "la transferencia se realizo con exito."
                	END
                END
            END
            ELSE
            BEGIN
            	print "No cuenta con la cantidad necesaria"
            END
        END  
        ELSE IF (@tempStatus = 'unfonded')
        BEGIN
                	print "La cuenta no tiene fondos"
		END
		ELSE IF (@tempStatus = 'locked')
        BEGIN
            print "La cuenta esta bloqueada"
		END
		ELSE IF (@tempStatus = 'closed')
        BEGIN
            print "La cuenta esta cerrada"
		END
	END
	ELSE IF @atran is NULL
	BEGIN
    	print "No se envio No. de transferencia"
	END
	ELSE
	BEGIN
    	print "Numero no valido"
	END
END
ELSE
BEGIN
	print "no existe cuenta"
	rollback transaction
END
commit transaction
return        
go

---------------------Ejemplos ATM

--Deposito
exec atm '1273588-4', 10, 10
--Retiro
exec atm '1273588-4', 20, 10  
--Transferencia
exec atm '1273588-4', 50, 10, '1274495-6'



---Ejemplo Movs
exec checkAccountMoves '1273588-4'

---------------------