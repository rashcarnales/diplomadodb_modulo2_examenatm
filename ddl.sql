﻿--Borrar typo de dato
sp_droptype cct
go


-- Crear un tipo de dato para las cuentas
sp_addtype cct, "char(12)"
go


--Borrar 
DROP TABLE cuentas
go


--Sacar una copia de la tabla de cuentas.
select * 
into cuentas1 
from cuentas
go




--Crear
DROP TABLE cuentas
go
CREATE TABLE cuentas(
        acct_no cct not null,
        acc_type char(3) not null,
        status varchar(15) not null,
        cur_bal numeric(10,2) not null,
        PRIMARY KEY(acct_no)
)
go


--Borrar 
DROP TABLE trans
go


CREATE TABLE trans(
        tran_id int,
        tran_name varchar(21) not null,
        PRIMARY KEY(tran_id)
)
go


--Borrar 
DROP TABLE movs
go


CREATE TABLE movs(
        id_mov numeric(5,0) IDENTITY,
        acct_no cct not null,
        tran_id int,
        fecha_mov date default getdate(),
		monto numeric(10,2) null, 
        PRIMARY KEY(id_mov),
        FOREIGN KEY(acct_no ) REFERENCES cuentas(acct_no),
        FOREIGN KEY(tran_id ) REFERENCES trans(tran_id)
)
go


Subquery pg 1.39 1.72
Wildcard pg 1.43
Custom data type 1.86


indices 


Identity 1.91


if ex