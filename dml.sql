﻿INSERT INTO trans (tran_id, tran_name ) VALUES (10, 'Deposito')
go
INSERT INTO trans (tran_id, tran_name ) VALUES (20, 'Retiro')
go
INSERT INTO trans (tran_id, tran_name ) VALUES (30, 'Consulta Saldo')
go 
INSERT INTO trans (tran_id, tran_name ) VALUES (40, 'Consulta de Movimientos')
go 
INSERT INTO trans (tran_id, tran_name ) VALUES (50, 'Transferencia')
go

INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1135899-8', 'CH1', 'Closed', 0)
go
INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1273588-4',   	'CH1',	'Active',	786789)
go
INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1273708-9',   'CH1',	'Locked',	12789)
go
INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1274495-6',   	'CH3',	'Active',	8526)
go
INSERT INTO cuentas(acct_no, acc_type, status, cur_bal) VALUES ('1274540-5',   	'CH3',	'Unfunded',	0)
go