﻿---------------------Borrado de store procedure
drop proc deposit
go
drop proc withdrawal
go
drop proc checkAccountMoves
go
drop proc atm
go

---------------------Deposito
create proc deposit
@acct cct,
@amonto int
as


declare @tempRowCount1 int
declare @tempRowCount2 int
declare @tempNewMonto numeric(10,2)


INSERT INTO movs (acct_no, tran_id,  monto ) VALUES (@acct,  10, @amonto)
SELECT @tempRowCount1 = @@rowcount
print "@tempRowCount1 %1!", @tempRowCount1


IF @amonto > 0
BEGIN
	UPDATE cuentas set cur_bal = cur_bal + @amonto, status = 'Active' WHERE acct_no=@acct
	SELECT @tempRowCount2 = @@rowcount
	print "@tempRowCount2 %1!", @tempRowCount2

	SELECT acct_no, cur_bal FROM cuentas WHERE acct_no=@acct
	IF @@error <> 0 or (@tempRowCount1 <> 1 AND @tempRowCount2 <> 1)
	BEGIN
		print 'deposito no realizado'
                                
   		return -900
	END
	ELSE
	BEGIN
    	select @tempNewMonto=(select cur_bal from cuentas where acct_no=@acct)
		print "su saldo actual es de %1!", @tempNewMonto
		return 0
	END
END
ELSE
BEGIN
	print "el monto a depositar debe ser mayor a 0"
END 
go

---------------------Retiro
create proc withdrawal
@acct cct,
@amonto int
as


declare @tempRowCount1 int
declare @tempRowCount2 int
declare @tempNewMonto numeric(10,2)


INSERT INTO movs (acct_no, tran_id,  monto ) VALUES (@acct,  20, @amonto)
SELECT @tempRowCount1 = @@rowcount
print "@tempRowCount1 %1!", @tempRowCount1


UPDATE cuentas set cur_bal = cur_bal - @amonto WHERE acct_no=@acct
SELECT @tempRowCount2 = @@rowcount
print "@tempRowCount2 %1!", @tempRowCount2

SELECT acct_no, @tempNewMonto = cur_bal FROM cuentas WHERE acct_no=@acct
IF @@error <> 0 or (@tempRowCount1 <> 1 AND @tempRowCount2 <> 1)
BEGIN
	print 'retiro no realizado'
                                
    return -901
END
ELSE
BEGIN
	IF @tempNewMonto = 0
	BEGIN
		UPDATE cuentas set status = 'Unfunded' WHERE acct_no=@acct
	END
    select @tempNewMonto=(select cur_bal from cuentas where acct_no=@acct)
	print "su saldo actual es de %1!", @tempNewMonto
	return 0
END
go

---------------------Consulta de Movimientos
create proc checkAccountMoves
@acct cct
as

DECLARE movscur CURSOR
	FOR SELECT movs.fecha_mov, movs.monto, trans.tran_name FROM movs, trans WHERE movs.tran_id = trans.tran_id AND movs.acct_no = @acct ORDER BY 1 for read only

declare @fecha date, @monto numeric(10,2), @name varchar(21)
open movscur	
fetch movscur into @fecha, @monto, @name
while @@sqlstatus <> 2
begin
	if @@sqlstatus = 1
	begin
		--raiserror 30001 "select failed"
		 print "Error con el select"
 		 close movscur
 		 deallocate cursor movscur
 		 return
	end
	--fetch movscur into @fecha, @monto, @name
	print "%1! cantidad  %2! transaccion %3! :", @fecha, @monto, @name
	fetch movscur into @fecha, @monto, @name
end
close movscur 
deallocate cursor movscur
go

---------------------ATM
create proc atm 
@acct1 cct, 
@atran int  = NULL,  
@amonto numeric(10,2)  = NULL,  
@acct2 cct  = NULL,  
@rest int  = NULL
as


begin transaction


declare @tempNewMonto numeric(10,2)
declare @tempRowCount int
declare @tempRowCount1 int
declare @tempRowCount2 int
declare @tempAcct_no cct
declare @tempAcc_type char(3)
declare @tempStatus varchar(15) 
declare @tempCur_bal numeric(10,2)


SELECT @tempAcct_no = acct_no, @tempAcc_type = acc_type, @tempStatus = lower(status), @tempCur_bal = cur_bal FROM cuentas WHERE acct_no = @acct1


SELECT @tempRowCount = @@rowcount


IF @tempRowCount=1
BEGIN
    print "existe cuenta"
    IF @atran=10
    BEGIN
		print "deposito"
    	print @tempStatus

    	IF (@tempStatus = 'active' OR @tempStatus = 'unfonded' OR @tempStatus= 'locked' )
        BEGIN
        	declare @restDeposit int
                	
            exec @restDeposit =  deposit @acct1, @amonto
            IF @restDeposit<> 0
            BEGIN
            	print "Error depositando"
            END
            ELSE
            BEGIN
            	print "El deposito se realizo con exito."
            END     
        END
        ELSE IF (@tempStatus = 'closed')
        BEGIN
        	print "La cuenta esta cerrada"
		END
	END
    ELSE IF @atran = 20
    BEGIN
		print "retiro"
        print @tempStatus

        IF (@tempStatus = 'active')
        BEGIN
                
        	IF @tempCur_bal >= @amonto
            BEGIN
            	declare @restWDrawal int
                	
                exec @restWDrawal =  withdrawal @acct1, @amonto
                IF @restWDrawal<> 0
                BEGIN
                	print "Error al hacer el retiro"
                END
                ELSE
                BEGIN
                	print "El retiro se realizo con exito."
                END
            END
            ELSE
            BEGIN
            	print "No cuenta con la cantidad necesaria"
            END
        END  
        ELSE IF (@tempStatus = 'unfonded')
        BEGIN
                	print "La cuenta no tiene fondos"
		END
		ELSE IF (@tempStatus = 'locked')
        BEGIN
            print "La cuenta esta bloqueada"
		END
		ELSE IF (@tempStatus = 'closed')
        BEGIN
            print "La cuenta esta cerrada"
		END
	END
	ELSE IF @atran = 30
	BEGIN
		print "consulta de saldo"
		print "su saldo actual es de %1!", @tempCur_bal
	END        
	ELSE IF @atran = 40
	BEGIN
		print "Consulta movimientos"
    	print @tempStatus

    	IF (@tempStatus = 'active' OR @tempStatus = 'unfonded' OR @tempStatus= 'locked' )
        BEGIN
        	  exec checkAccountMoves @acct1
        END
        ELSE IF (@tempStatus = 'closed')
        BEGIN
        	print "La cuenta esta cerrada"
		END
	END
	ELSE IF @atran = 50
	BEGIN
    	print "transferencia"
        print @tempStatus

        IF (@tempStatus = 'active')
        BEGIN
                
        	IF @tempCur_bal >= @amonto
            BEGIN
            	declare @restTran1 int
                	
                exec @restTran1 =  withdrawal @acct1, @amonto
                IF @restTran1<> 0
                BEGIN
                	print "Error al hacer el retiro de tu cuenta durante la transferecia"
                END
                ELSE
                BEGIN
                
                	declare @restTran2 int
                	exec @restTran2 =  deposit @acct2, @amonto
                	IF @restTran2<> 0
                	BEGIN
                		print "Error al hacer el deposito durante la transferecia"
                	END
                	ELSE
                	BEGIN
                		print "la transferencia se realizo con exito."
                	END
                END
            END
            ELSE
            BEGIN
            	print "No cuenta con la cantidad necesaria"
            END
        END  
        ELSE IF (@tempStatus = 'unfonded')
        BEGIN
                	print "La cuenta no tiene fondos"
		END
		ELSE IF (@tempStatus = 'locked')
        BEGIN
            print "La cuenta esta bloqueada"
		END
		ELSE IF (@tempStatus = 'closed')
        BEGIN
            print "La cuenta esta cerrada"
		END
	END
	ELSE IF @atran is NULL
	BEGIN
    	print "No se envio No. de transferencia"
	END
	ELSE
	BEGIN
    	print "Numero no valido"
	END
END
ELSE
BEGIN
	print "no existe cuenta"
	rollback transaction
END
commit transaction
return        
go

---------------------Ejemplos ATM

--Deposito
exec atm '1273588-4', 10, 10
--Retiro
exec atm '1273588-4', 20, 10  
--Transferencia
exec atm '1273588-4', 50, 10, '1274495-6'



---Ejemplo Movs
exec checkAccountMoves '1273588-4'

---------------------
declare @num_rows int




select @num_rows = @@rowcount


select * from cuentas where acct_no = ‘1135899-8'


declare @temp int




SELECT @temp = titles.price FROM titles WHERE title_id =  @idLibro


SELECT @total = SUM(@temp *  salesdetail.qty) FROM salesdetail, sales WHERE salesdetail.title_id =  @idLibro AND salesdetail.ord_num = sales.ord_num AND sales.date = @dateLibro


SELECT movs.fecha_mov, movs.monto, trans.tran_name FROM movs, trans WHERE movs.tran_id = trans.tran_id AND movs.acct_no = '1273588-4' ORDER BY 1

return




date type 3.9
declare variable 3.20


--Devuelve los renglones afectados
select @@rowcount


-- 0 cuando no hay error
select @@error


------------Indice de temas en las Presentaciones.


2.10  Case
2.16  View 
3.32 IF
3.37 sysobject
3.44 print
3.47 error
3.53 tran
3.70 error con tran
4.3 cursor
4.7 variables global en cursor
4.17 sp_cursorinfo
4.25 store procedure
4.27 sp_helptext procedure_name
4.33 ejemplo param null en procedure
4.40 ejemplo grande de store procedure
4.49, 4.52 ejemplo devolviendo 0
4.58 trigger
4.78 usos de @@rowcount  


